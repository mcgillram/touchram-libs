package ca.mcgill.gesturecreator;

import org.mt4j.MTApplication;
import org.mt4j.components.visibleComponents.shapes.MTPolygon;
import org.mt4j.components.visibleComponents.shapes.MTRectangle;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vertex;

/**
 * 
 * Rectangle showing a gesture that was drawn.
 * 
 * @author Franz
 */
public class DrawnGestureRectangle extends MTRectangle {
    private MTApplication application;
    private MTPolygon drawn_gesture;
    private Vertex[] drawn_points;
    private float width;
    private float height;
    private float pos_x;
    private float pos_y;

    /**
     * Constructor.
     * 
     * @param mtapp The application.
     * @param pWidth Width of the rectangle.
     * @param pHeight Height of the rectangle
     */
    public DrawnGestureRectangle(MTApplication mtapp, int pWidth, int pHeight) {
        // Sets this rectangle to the bottom right of the application.
        super(mtapp, mtapp.getWidth() - pWidth, mtapp.getHeight() - pHeight, pWidth, pHeight);
        application = mtapp;
        width = pWidth;
        height = pHeight;
        pos_x = mtapp.getWidth() - pWidth;
        pos_y = mtapp.getHeight() - pHeight;
        drawn_gesture = buildDrawnGesture(new Vertex[] {});
        // Ignore all events
        this.setEnabled(false);
    }

    /**
     * Draw the gesture in the rectangle. Removes previous drawn gesture.
     * 
     * @param points Points representing the gesture.
     */
    public void setDrawnGesture(Vertex[] points) {
        this.removeChild(drawn_gesture);
        this.drawn_gesture = buildDrawnGesture(points);
        this.addChild(drawn_gesture);
    }

    /**
     * Get points of the gesture that is currently drawn.
     * 
     * @return Array of vertices corresponding to the drawn gesture.
     */
    public Vertex[] getDrawnPoints() {
        return drawn_points;
    }

    private MTPolygon buildDrawnGesture(Vertex[] points) {
        Vertex[] vertices = new Vertex[points.length];
        int i = 0;
        for (Vertex v : points) {
            // Scale the gesture to the size of this rectangle.
            vertices[i] = new Vertex(width * v.x / application.getWidth() + pos_x,
                    height * v.y / application.getHeight() + pos_y);
            i++;
        }

        // Build the MTPolygon corresponding to the gesture
        MTPolygon drawn_ges = new MTPolygon(application, vertices);
        drawn_ges.setNoFill(true);
        drawn_ges.setNoStroke(false);
        drawn_ges.setStrokeColor(MTColor.BLACK);
        drawn_ges.setEnabled(false);
        this.drawn_points = points;
        return drawn_ges;
    }
}
