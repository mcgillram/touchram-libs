package org.tigris.geflayout.sugiyama.ram;



/**
 * @author Subtain
 */
public class ClassStructuralProperties implements Comparable<ClassStructuralProperties>
{
    
    int iID;
    
    String sName;
    
    private float fHeight;
    private float fWidth;
    
    private float iXcord;
    private float iYcord;
    
    private int iLevel;
    private int iOrder;
    
    /**
     * @param fHeight
     * @param fWidth
     * @param iXcord
     * @param iYcord
     * @param iLevel
     * @param iOrder
     */
    public ClassStructuralProperties(int iID, String sName, float fHeight, float fWidth, float iXcord, float iYcord, int iLevel, int iOrder)
    {
        super();
        this.iID = iID;
        this.sName = sName;
        this.fHeight = fHeight;
        this.fWidth = fWidth;
        this.iXcord = iXcord;
        this.iYcord = iYcord;
        this.iLevel = iLevel;
        this.iOrder = iOrder;
    }
    
    @Override
    public int compareTo(ClassStructuralProperties arg0)
    {
        int iID = arg0.getiID();
        
        // ascending order
        return this.iID - iID;
    }
    
    /**
     * @return
     */
    public float getfHeight()
    {
        return fHeight;
    }
    
    /**
     * @return
     */
    public float getfWidth()
    {
        return fWidth;
    }
    
    public int getiID()
    {
        return iID;
    }
    
    /**
     * @return
     */
    public int getiLevel()
    {
        return iLevel;
    }
    
    /**
     * @return
     */
    public int getiOrder()
    {
        return iOrder;
    }
    
    /**
     * @return
     */
    public float getiXcord()
    {
        return iXcord;
    }
    
    public float getiYcord()
    {
        return iYcord;
    }
    
    public String getsName()
    {
        return sName;
    }
    
    public void setfHeight(float fHeight)
    {
        this.fHeight = fHeight;
    }
    
    public void setfWidth(float fWidth)
    {
        this.fWidth = fWidth;
    }
    
    public void setiID(int iID)
    {
        this.iID = iID;
    }
    
    public void setiLevel(int iLevel)
    {
        this.iLevel = iLevel;
    }
    
    public void setiOrder(int iOrder)
    {
        this.iOrder = iOrder;
    }
    
    public void setiXcord(float iXcord)
    {
        this.iXcord = iXcord;
    }
    
    public void setiXcord(int iXcord)
    {
        this.iXcord = iXcord;
    }
    
    public void setiYcord(float iYcord)
    {
        this.iYcord = iYcord;
    }
    
    public void setiYcord(int iYcord)
    {
        this.iYcord = iYcord;
    }
    
    public void setsName(String sName)
    {
        this.sName = sName;
    }
    
}
