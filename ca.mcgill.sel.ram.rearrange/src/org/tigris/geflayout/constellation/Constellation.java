// $Id: Constellation.java 106 2006-08-05 03:32:23Z bobtarling $
// Copyright (c) 2006 The Regents of the University of California. All
// Rights Reserved. Permission to use, copy, modify, and distribute this
// software and its documentation without fee, and without a written
// agreement is hereby granted, provided that the above copyright notice
// and this paragraph appear in all copies.  This software program and
// documentation are copyrighted by The Regents of the University of
// California. The software program and documentation are supplied "AS
// IS", without any accompanying services from The Regents. The Regents
// does not warrant that the operation of the program will be
// uninterrupted or error-free. The end-user understands that the program
// was developed for research purposes and is advised not to rely
// exclusively on the program for any reason.  IN NO EVENT SHALL THE
// UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
// SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS,
// ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
// THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF
// SUCH DAMAGE. THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
// PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
// CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
// UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

package org.tigris.geflayout.constellation;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tigris.geflayout.layout.Layouter;
import org.tigris.geflayout.layout.LayouterFactory;
import org.tigris.geflayout.layout.LayouterNode;
import org.tigris.geflayout.layout.LayouterObject;

public class Constellation implements LayouterNode, Layouter {
    
    private Log log = LogFactory.getLog(Constellation.class);
    
    private List objects = new ArrayList();
    
    private LayouterFactory layouterFactory;
    
    public static int X_GAP = 50;
    
    public static int Y_GAP = 50;
    
    public Constellation(LayouterFactory layouterFactory) {
        if (layouterFactory == null) {
            throw new IllegalArgumentException("A LayouterFactory must be supplied");
        }
        this.layouterFactory = layouterFactory;
    }
    
    public Point getLocation() {
        Rectangle rect = getBounds();
        return new Point(rect.x, rect.y);
    }

    public Dimension getSize() {
        return getBounds().getSize();
    }

    public Rectangle getBounds() {
        Rectangle rect = null;
        for (Iterator it = getObjects().iterator(); it.hasNext(); ) {
            Object obj = it.next();
            if (obj instanceof LayouterNode) {
                if (rect == null) {
                    rect = ((LayouterNode) obj).getBounds();
                } else {
                    rect.add(((LayouterNode) obj).getBounds());
                }
            }
        }
        log.info("Getting bounds of " + getObjects().size() + " objects as " + rect);
        return rect;
    }

    public void setLocation(Point newLocation) {
        Point currentLocation = getLocation();
        int dx = newLocation.x - currentLocation.x;
        int dy = newLocation.y - currentLocation.y;
        translate(dx, dy);
    }
    
    public void translate(int dx, int dy) {
        for (Iterator it = getObjects().iterator(); it.hasNext(); ) {
            LayouterObject obj = (LayouterObject) it.next();
            obj.translate(dx, dy);
        }
    }

    /**
     * @see org.tigris.geflayout.layout.Layouter#add(org.tigris.geflayout.layout.LayouterObject)
     */
    public void add(LayouterObject obj) {
        log.debug("Adding " + obj.getClass().getName() + " to Constellation");
        objects.add(obj);
    }

    public List getObjects() {
        return objects;
    }
    
    public Object getContent() {
        return objects;
    }

    public void layout() {
        log.info("Creating layouter within Constellation with " + objects.size() + " " + layouterFactory.getClass().getName());
        Layouter layouter = layouterFactory.createLayouter(objects);
        log.info("Laying out contents of Constellation with " + objects.size() + " " + layouter.getClass().getName());
        layouter.layout();
    }
    
    public boolean contains(Object layouterObject) {
        return objects.contains(layouterObject);
    }
}
