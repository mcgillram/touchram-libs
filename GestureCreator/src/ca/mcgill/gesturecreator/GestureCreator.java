package ca.mcgill.gesturecreator;

import org.mt4j.MTApplication;

/**
 * Create Gestures application.
 * 
 * @author Franz
 */
public class GestureCreator extends MTApplication {

    private static final long serialVersionUID = 8753433170100668811L;

    public static void main(String[] args) {
        initialize();
    }

    @Override
    public void startUp() {
        addScene(new GestureRecognizerScene(this, "GestureCreator"));
    }

}
