package org.tigris.geflayout.sugiyama.ram;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.tigris.gef.presentation.FigNode;
import org.tigris.gef.presentation.FigRect;
import org.tigris.geflayout.sugiyama.SugiyamaEdge;
import org.tigris.geflayout.sugiyama.SugiyamaLayouter;
import org.tigris.geflayout.sugiyama.SugiyamaNode;


/**
 * @author Subtain
 */
public class UmlClassArrangement
{
    
    HashMap<String, List<String>> SiblingParentPair;
    
    HashMap<String, String> SelectedSiblingParentPair;  // <child, parent>
    
    HashMap<SugiyamaNode, SugiyamaNode> SelectedSiblingParentSugiyamaNodePair;  // <child, parent>
    
    List<FigNode> FigNodeList;
    
    List<SugiyamaNode> SugiyamaNodeList;
    
    List<ClassStructuralProperties> currentClassObects;
    SugiyamaLayouter layouter;
    
    /**************** Constructor **********************************/
    
    public UmlClassArrangement(List<ClassStructuralProperties> currentClassObects, HashMap<String, List<String>> SiblingParentPair)
    {
        this.SiblingParentPair = SiblingParentPair;
        this.currentClassObects = currentClassObects;
        
        FigNodeList = new ArrayList<FigNode>();
        SugiyamaNodeList = new ArrayList<SugiyamaNode>();
        // currentClassObects = new ArrayList<ClassStructuralProperties>();
        SelectedSiblingParentPair = new HashMap<String, String>();
        
        SelectedSiblingParentSugiyamaNodePair = new HashMap<SugiyamaNode, SugiyamaNode>();
        
        layouter = new SugiyamaLayouter()
        {};
        
    }
    
    protected void addSugiyamaEdges()
    {
        
        Iterator<SugiyamaNode> iteratorSugiyamaNodeList = SugiyamaNodeList.iterator();
        Iterator<SugiyamaNode> innerIteratorSugiyamaNodeList;
        
        SugiyamaNode childSugiyamaNode;
        
        SugiyamaNode parentSugiyamaNode;
        
        String sChildClassName;
        String sTempParentClassName;
        
        while (iteratorSugiyamaNodeList.hasNext()) {
            childSugiyamaNode = iteratorSugiyamaNodeList.next();
            
            sChildClassName = childSugiyamaNode.getNodeName();
            
            /* For now only using selected sibling pair... apres use all the list. We do NOT want to lose and edge */
            
            if (SiblingParentPair.containsKey(sChildClassName)) {
                List<String> siblingParentPairList = SiblingParentPair.get(sChildClassName);
                
                Iterator<String> siblingParentPairListIterator = siblingParentPairList.iterator();
                
                while (siblingParentPairListIterator.hasNext()) {
                    sTempParentClassName = siblingParentPairListIterator.next();
                    
                    // Reseting the iterator
                    
                    innerIteratorSugiyamaNodeList = SugiyamaNodeList.iterator();
                    
                    while (innerIteratorSugiyamaNodeList.hasNext()) {
                        parentSugiyamaNode = innerIteratorSugiyamaNodeList.next();
                        
                        if (parentSugiyamaNode.getNodeName().equalsIgnoreCase(sTempParentClassName)) {
                            
                            layouter.add(new SugiyamaEdge(parentSugiyamaNode, childSugiyamaNode));
                            System.out.println(parentSugiyamaNode.getNodeName() + "--" + childSugiyamaNode.getNodeName());
                            
                        }
                    }
                    
                }
            }
            
            /*
            if (SelectedSiblingParentPair.containsKey(sChildClassName)) {
                sTempParentClassName = SelectedSiblingParentPair.get(sChildClassName);
                
                // Reseting the iterator
                
                innerIteratorSugiyamaNodeList = SugiyamaNodeList.iterator();
                
                while (innerIteratorSugiyamaNodeList.hasNext()) {
                    parentSugiyamaNode = innerIteratorSugiyamaNodeList.next();
                    
                    if (parentSugiyamaNode.getNodeName().equalsIgnoreCase(sTempParentClassName)) {
                        
                        layouter.add(new SugiyamaEdge(parentSugiyamaNode, childSugiyamaNode));
                        System.out.println(parentSugiyamaNode.getNodeName() + "--" + childSugiyamaNode.getNodeName());
                        
                    }
                }
                
            }*/
        }
        
    }
    
    /**
     * @param currentClassObects
     * @param SiblingParentPair
     * @return List<ClassStructuralProperties>
     */
    public List<ClassStructuralProperties> executeLayoutAlgorithm()
    {
        
        /* Since the  algorithm handles only one parent so given a child, one parent is randomly selected */
        
        populateSelectedSiblingParentPair();
        
        SugiyamaNode new_SugiyamaNode;
        
        ClassStructuralProperties classStructuralProperty;
        
        Iterator<ClassStructuralProperties> iteratorClassStructuralProperties = currentClassObects.iterator();
        
        while (iteratorClassStructuralProperties.hasNext()) {
            
            classStructuralProperty = iteratorClassStructuralProperties.next();
            
            new_SugiyamaNode = new SugiyamaNode(new FigNode(new FigRect(10, 10, 16, 16)));
            
            new_SugiyamaNode.setName(classStructuralProperty.getsName());
            // new_SugiyamaNode.setX((int) classStructuralProperty.getiXcord());
            // new_SugiyamaNode.setY((int) classStructuralProperty.getiYcord());
            new_SugiyamaNode.setfHeight(classStructuralProperty.getfHeight());
            new_SugiyamaNode.setfWidth(classStructuralProperty.getfWidth());
            
            /* Used to updated root nodes relationship */
            
            SugiyamaNodeList.add(new_SugiyamaNode);
            
        }
        
        /* Add the parents to the Sugiyama node here */
        
        populateSelectedSiblingParentSugiyamaNodePair();
        
        Iterator<SugiyamaNode> iteratorSugiyamaNodeList = SugiyamaNodeList.iterator();
        
        while (iteratorSugiyamaNodeList.hasNext()) {
            
            layouter.add(iteratorSugiyamaNodeList.next());
            
        }
        
        addSugiyamaEdges();
        
        layouter.layout();
        
        updateClassObjectList();
        
        return currentClassObects;
        
    }
    
    /*
     * This populates the Hashmap for selected sibling-parent pair, so that adding the parent Sugiyama Node to the child Sugiyama node is done in O(1) time
     */
    protected void populateSelectedSiblingParentPair()
    {
        
        Iterator<ClassStructuralProperties> iteratorClassStructuralProperties = currentClassObects.iterator();
        ClassStructuralProperties tempClassStructuralProperty;
        
        String sTempClassName;
        
        while (iteratorClassStructuralProperties.hasNext()) {
            tempClassStructuralProperty = iteratorClassStructuralProperties.next();
            
            if (SiblingParentPair.containsKey(tempClassStructuralProperty.getsName())) {
                
                sTempClassName = tempClassStructuralProperty.getsName();
                
                /* Iterate through the list and make random number and all that jazz.. for later */
                
                int iSize = SiblingParentPair.get(sTempClassName).size();
                
                Random generator = new Random();
                int iRandomIndex = generator.nextInt(iSize);
                
                SelectedSiblingParentPair.put(sTempClassName, SiblingParentPair.get(sTempClassName).get(iRandomIndex));
                
            }
            
        }
        
    }
    
    protected void populateSelectedSiblingParentSugiyamaNodePair()
    {
        
        Iterator<SugiyamaNode> iteratorSugiyamaNodeList = SugiyamaNodeList.iterator();
        Iterator<SugiyamaNode> innerIteratorSugiyamaNodeList;
        SugiyamaNode tempSugiyamaNode;
        
        SugiyamaNode innerTempSugiyamaNode;
        
        String sChildClassName;
        String sTempParentClassName;
        
        while (iteratorSugiyamaNodeList.hasNext()) {
            tempSugiyamaNode = iteratorSugiyamaNodeList.next();
            sChildClassName = tempSugiyamaNode.getNodeName();
            
            if (SelectedSiblingParentPair.containsKey(sChildClassName)) {
                sTempParentClassName = SelectedSiblingParentPair.get(sChildClassName);
                
                /* Reseting the iterator */
                
                innerIteratorSugiyamaNodeList = SugiyamaNodeList.iterator();
                
                while (innerIteratorSugiyamaNodeList.hasNext()) {
                    innerTempSugiyamaNode = innerIteratorSugiyamaNodeList.next();
                    
                    if (innerTempSugiyamaNode.getNodeName().equalsIgnoreCase(sTempParentClassName)) {
                        
                        tempSugiyamaNode.setRoot(innerTempSugiyamaNode);
                        // layouter.add(new SugiyamaEdge(innerTempSugiyamaNode, tempSugiyamaNode));
                        
                    }
                }
                
            }
        }
    }
    
    /**
     * 
     */
    public void printNodes()
    {
        Iterator<SugiyamaNode> itr = SugiyamaNodeList.iterator();
        
        SugiyamaNode temp;
        
        while (itr.hasNext()) {
            temp = itr.next();
            
            System.out.println("Level: " + temp.getLevel());
            System.out.println("Order: " + temp.getOrder());
            System.out.println("X-cord: " + temp.getX());
            System.out.println("Y-cord: " + temp.getY());
            System.out.println("=============");
        }
        
    }
    
    private void updateClassObjectList()
    {
        Iterator<SugiyamaNode> itr = SugiyamaNodeList.iterator();
        
        SugiyamaNode temp;
        
        int i = 0;
        
        while (itr.hasNext()) {
            temp = itr.next();
            
            currentClassObects.get(i).setiXcord(temp.getX());
            currentClassObects.get(i).setiYcord(temp.getY());
            
            ++i;
        }
    }
}
