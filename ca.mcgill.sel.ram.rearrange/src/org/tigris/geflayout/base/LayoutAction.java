// $Id: LayoutAction.java 32 2006-07-04 14:50:54Z harrigan $
// Copyright (c) 2006 The Regents of the University of California. All
// Rights Reserved. Permission to use, copy, modify, and distribute this
// software and its documentation without fee, and without a written
// agreement is hereby granted, provided that the above copyright notice
// and this paragraph appear in all copies.  This software program and
// documentation are copyrighted by The Regents of the University of
// California. The software program and documentation are supplied "AS
// IS", without any accompanying services from The Regents. The Regents
// does not warrant that the operation of the program will be
// uninterrupted or error-free. The end-user understands that the program
// was developed for research purposes and is advised not to rely
// exclusively on the program for any reason.  IN NO EVENT SHALL THE
// UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
// SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS,
// ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
// THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF
// SUCH DAMAGE. THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
// PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
// CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
// UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

package org.tigris.geflayout.base;

import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.Vector;

import org.tigris.gef.base.Editor;
import org.tigris.gef.base.Globals;
import org.tigris.gef.base.SelectionManager;
import org.tigris.gef.graph.MutableGraphSupport;
import org.tigris.gef.presentation.Fig;
import org.tigris.gef.presentation.FigEdge;
import org.tigris.gef.presentation.FigNode;
import org.tigris.gef.undo.UndoableAction;
import org.tigris.geflayout.layout.Layouter;
import org.tigris.geflayout.sugiyama.SugiyamaEdge;
import org.tigris.geflayout.sugiyama.SugiyamaNode;

public class LayoutAction extends UndoableAction {
    
    Layouter layouter;
    
    /**
     * Construct an Action with some Layouter.
     * 
     * @param aLayouter
     */
    public LayoutAction(Layouter aLayouter) {
        super("Layout");
        layouter = aLayouter;
    }

    /**
     * Perform the layout action.
     */
    public void actionPerformed(ActionEvent e) {

        super.actionPerformed(e);
        
        Editor ce = Globals.curEditor();
        SelectionManager sm = ce.getSelectionManager();
        if (sm.getLocked()) {
            Globals.showStatus("Cannot Modify Locked Objects");
            return;
        }
        
        Vector figs = sm.getFigs();
        
        if (figs.size() == 0) {
            Globals.showStatus("Nothing Selected");
            return;
        }
        
        Iterator it = figs.iterator();
        while (it.hasNext()) {
            Fig f = (Fig) it.next();
            if (f instanceof FigNode)
                layouter.add(new SugiyamaNode((FigNode)f));
        }
        
        it = figs.iterator();
        while (it.hasNext()) {
            Fig f = (Fig) it.next();
            if (f instanceof FigEdge)
                layouter.add(new SugiyamaEdge((FigEdge)f));
        }
       
        layouter.layout();

        MutableGraphSupport.enableSaveAction();
        sm.endTrans();
    }
}
