package ca.mcgill.gesturecreator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils;
import org.mt4j.util.math.Vertex;

public class DrawnGestureSaver {

    public static boolean saveToFile(String path, Vertex[] vertices, boolean overwrite) {
        File new_file = new File(path);
        if (!new_file.exists()) {
            try {
                new_file.createNewFile();
            } catch (IOException e) {
                return false;
                // error TODO
            }
        } else if (!overwrite) {
            // error TODO
            return false;
        }
        PrintStream printw = null;
        try {
            // Use the recorder from MT4J and save the output to the file
            PrintStream stdout = System.out;
            printw = new PrintStream(new_file);
            System.setOut(printw);
            UnistrokeUtils util = new UnistrokeUtils();
            util.getRecorder().record(GestureCreatorUtils.convertVerticesToVector3DList(vertices));
            System.setOut(stdout);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (printw != null) {
                printw.close();
            }
        }
        return false;

    }

}
