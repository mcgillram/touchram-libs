package ca.mcgill.gesturecreator;

import java.util.ArrayList;
import java.util.List;

import org.mt4j.MTApplication;
import org.mt4j.components.TransformSpace;
import org.mt4j.components.visibleComponents.shapes.MTPolygon;
import org.mt4j.components.visibleComponents.widgets.buttons.MTImageButton;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeEvent;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.Direction;
import org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor.UnistrokeUtils.UnistrokeGesture;
import org.mt4j.sceneManagement.AbstractScene;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;

public class GestureRecognizerScene extends AbstractScene {

    private List<Vector3D> all_points = new ArrayList<Vector3D>();
    private DrawnGestureRectangle drawnGestureRect;
    private boolean testMode = false;
    private MTImageButton saveButton;
    private MTImageButton testgestbutton;

    public GestureRecognizerScene(final MTApplication mtapp, String name) {
        super(mtapp, name);
        buildBottomRightRectangle(mtapp);
        buildSaveGestureButton(mtapp);
        buildTestGestureButton(mtapp);
    }

    private void buildBottomRightRectangle(MTApplication mtapp) {
        // Create the drawn gesture rectangle at bottom right.
        drawnGestureRect = new DrawnGestureRectangle(mtapp, 250, 250);
        getCanvas().addChild(drawnGestureRect);
        final UnistrokeProcessor u = new UnistrokeProcessor(mtapp);

        // Create a gesture listener
        IGestureEventListener listener = new IGestureEventListener() {

            @Override
            public boolean processGestureEvent(MTGestureEvent ge) {
                if (ge instanceof UnistrokeEvent) {
                    if (ge.getId() == MTGestureEvent.GESTURE_STARTED) {
                        getCanvas().addChild(((UnistrokeEvent) ge).getVisualization());
                    } else if (ge.getId() == MTGestureEvent.GESTURE_ENDED) {
                        MTPolygon drawn_gesture = (MTPolygon) getCanvas().getChildByName("Polygon");
                        if (drawn_gesture != null) {
                            getCanvas().removeChild(drawn_gesture);
                            if (!testMode) {
                                drawnGestureRect.setDrawnGesture(drawn_gesture.getVerticesLocal());
                                u.getRecognizer().clearRecognizerTemplates();
                                u.getRecognizer().setCustomGesture(GestureCreatorUtils
                                        .convertVerticesToVector3DList(drawnGestureRect.getDrawnPoints()));
                                u.addTemplate(UnistrokeGesture.CUSTOMGESTURE, Direction.CLOCKWISE);
                            }
                        }
                        if (testMode) {
                            System.out.println(((UnistrokeEvent) ge).getGesture() == UnistrokeGesture.CUSTOMGESTURE);
                        }
                    }
                }
                return false;
            }

        };
        this.getCanvas().unregisterAllInputProcessors();
        this.getCanvas().registerInputProcessor(u);
        this.getCanvas().addGestureListener(UnistrokeProcessor.class, listener);
    }

    private void buildSaveGestureButton(MTApplication mtapp) {
        saveButton = new MTImageButton(mtapp, mtapp.loadImage(GestureCreatorUtils.SAVE_BUTTON_LOCATION));
        saveButton.addGestureListener(TapProcessor.class, new IGestureEventListener() {

            @Override
            public boolean processGestureEvent(MTGestureEvent ge) {
                DrawnGestureSaver.saveToFile(GestureCreatorUtils.OUTPUT_LOCATION, drawnGestureRect.getDrawnPoints(),
                        true);
                return false;
            }
        });
        this.getCanvas().addChild(saveButton);
    }

    private void buildTestGestureButton(MTApplication mtapp) {
        testgestbutton = new MTImageButton(mtapp, mtapp.loadImage(GestureCreatorUtils.TEST_BUTTON_LOCATION));
        testgestbutton.removeAllGestureEventListeners();
        testgestbutton.addGestureListener(TapProcessor.class, new IGestureEventListener() {

            @Override
            public boolean processGestureEvent(MTGestureEvent ge) {
                if (ge instanceof TapEvent && ((TapEvent) ge).isTapDown()) {
                    toggleMode();
                    if (testMode) {
                        testgestbutton.setStrokeColor(MTColor.RED);
                    } else {
                        testgestbutton.setStrokeColor(MTColor.WHITE);
                    }
                }
                return testMode;
            }
        });
        testgestbutton
                .setPositionGlobal(new Vector3D(mtapp.getWidth() - testgestbutton.getWidthXY(TransformSpace.LOCAL) / 2,
                        testgestbutton.getHeightXY(TransformSpace.LOCAL) / 2));
        this.getCanvas().addChild(testgestbutton);
    }

    private void toggleMode() {
        testMode = !testMode;
    }

}
