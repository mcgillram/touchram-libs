// $Id: ConstellationLayouter.java 106 2006-08-05 03:32:23Z bobtarling $
// Copyright (c) 2006 The Regents of the University of California. All
// Rights Reserved. Permission to use, copy, modify, and distribute this
// software and its documentation without fee, and without a written
// agreement is hereby granted, provided that the above copyright notice
// and this paragraph appear in all copies.  This software program and
// documentation are copyrighted by The Regents of the University of
// California. The software program and documentation are supplied "AS
// IS", without any accompanying services from The Regents. The Regents
// does not warrant that the operation of the program will be
// uninterrupted or error-free. The end-user understands that the program
// was developed for research purposes and is advised not to rely
// exclusively on the program for any reason.  IN NO EVENT SHALL THE
// UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
// SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS,
// ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
// THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF
// SUCH DAMAGE. THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
// PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
// CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
// UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

package org.tigris.geflayout.constellation;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tigris.gef.presentation.FigEdge;
import org.tigris.gef.presentation.FigNode;
import org.tigris.geflayout.layout.Layouter;
import org.tigris.geflayout.layout.LayouterEdge;
import org.tigris.geflayout.layout.LayouterFactory;
import org.tigris.geflayout.layout.LayouterNode;
import org.tigris.geflayout.layout.LayouterObject;

/**
 * ClassDiagramLayouter2 creates multiple constellations of nodes and edges
 * and uses SugiyamaLayouter in order to layout each constellation.
 */
public class ConstellationLayouter implements Layouter {
    
    private static final Log log = LogFactory.getLog(ConstellationLayouter.class);
    
    /**
     * All the nodes
     */
    private Map layouterNodeByFig = new HashMap();
    
    /**
     * All the edges
     */
    private Map layouterEdgeByFig = new HashMap();
    
    /**
     * The constellations of nodes and edges
     */
    private ArrayList constellations = new ArrayList();
    
    /**
     * The constellations of nodes and edges
     */
    private ArrayList objects = new ArrayList();
    
    private LayouterFactory layouterFactory;
    
    /**
     * Construct the ConstellationLayouter
     * @param layouterFactory a factory class for creating the Layouter
     * for the internals of the constellation
     */
    public ConstellationLayouter(LayouterFactory layouterFactory) {
        this.layouterFactory = layouterFactory;
    }
    
    public void layout() {
        try {
            while (!layouterNodeByFig.isEmpty()) {
                FigNode figNode = (FigNode) layouterNodeByFig.keySet().iterator().next();
                Constellation constellation = new Constellation(layouterFactory);
                constellations.add(constellation);
                addFigToConstellation(figNode, constellation);
                constellation.layout();
            }
            positionConstellations();
        } catch (Exception e) {
            log.error("Exception caught", e);
        }
    }
    
    /**
     * Layout constellations in the most space efficient way. At the moment
     * we simply place them on top of eachother.
     * TODO: Determine widestConstellation and totalHeights
     * if (widestConstellation > (widestConsteallation + totalHeights) / 2) {
     *     targetWidth = widestConstellation
     * } else {
     *     targetWidth = (widestConsteallation + totalHeights) / 2
     * }
     * require an algorithm for placing the unconnected nodes in the most
     * space efficient way, limited to a total width of targetWidth.
     * 1. Place widest at start of current row
     * 2. Find widest that will fit remaining space (if none found the currentRow++ and goto (1)
     */
    private void positionConstellations() {
        
        ArrayList row = new ArrayList();
        
        int targetWidth = determineTargetWidth();
        log.info("Target width = " + targetWidth);
        
        int dy=0;
        int dx=0;
        
        int remainingWidth = targetWidth;
        
        do {
            Constellation constellation = removeWidestConstellation(remainingWidth);
            
            if (constellation == null) {
                // if there's not enough with to place the constellation in this
                // row then start a new row.
                
                int rowHeight = 0;
                for (Iterator it = row.iterator(); it.hasNext(); ) {
                    Constellation cons = (Constellation) it.next();
                    if (cons.getSize().height > rowHeight) {
                        rowHeight = cons.getSize().height;
                    }
                }
                
                dy += rowHeight + Constellation.Y_GAP;
                
                row = new ArrayList();
                dx = 0;
                remainingWidth = targetWidth;
                constellation = removeWidestConstellation(remainingWidth);
            }
            
            row.add(constellation);
            constellation.translate(dx, dy);
            dx += constellation.getSize().width + Constellation.X_GAP;
            remainingWidth -= constellation.getSize().width + Constellation.X_GAP;
            
        } while (!constellations.isEmpty());
    }
    
    private int determineTargetWidth() {
        // Get total height of constellations including gaps between
        // and the longest length
        int totalHeight = 0;
        int longestWidth = 0;
        for (Iterator it = constellations.iterator(); it.hasNext(); ) {
            Constellation constellation = (Constellation) it.next();
            Dimension constellationSize = constellation.getBounds().getSize();
            if (constellationSize.width > longestWidth) {
                longestWidth = constellationSize.width;
            }
            totalHeight += constellation.getBounds().height;
            if (it.hasNext()) {
                totalHeight += Constellation.Y_GAP;
            }
        }
        
        // The target width is half the total height
        
        int targetWidth = totalHeight / 2;
        
        // Unless that is less than the longest width
        
        if (targetWidth < longestWidth) {
            targetWidth = longestWidth;
        }
        return targetWidth;
    }
    
    /**
     * Remove the widest constealltion that will fit into the given width
     * @param width the available width
     * @return the constellation removed
     */
    private Constellation removeWidestConstellation(int width) {
        Constellation widestConstellation = null;
        for (Iterator it = constellations.iterator(); it.hasNext(); ) {
            Constellation constellation = (Constellation) it.next();
            if (constellation.getSize().width <= width) {
                if (widestConstellation == null) {
                    widestConstellation = constellation;
                } else if (constellation.getSize().width > widestConstellation.getSize().width) {
                    widestConstellation = constellation;
                }
            }
        }
        constellations.remove(widestConstellation);
        return widestConstellation;
    }
    
    
    private void addFigToConstellation(FigNode figNode, Constellation constellation) {
        LayouterNode layouterNode = (LayouterNode) layouterNodeByFig.get(figNode);
        if (layouterNode != null && !constellation.contains(layouterNode)) {
            addFigToConstellation(layouterNode, figNode, constellation);
        }
    }
    
    private void addFigToConstellation(LayouterNode layouterNode, FigNode figNode, Constellation constellation) {
        
        constellation.add(layouterNode);
        layouterNodeByFig.remove(figNode);
        List nodeEdges = new ArrayList(figNode.getFigEdges());
        for (Iterator it = nodeEdges.iterator(); it.hasNext(); ) {
            FigEdge figEdge = (FigEdge) it.next();
            LayouterEdge layouterEdge = (LayouterEdge) layouterEdgeByFig.get(figEdge);
            if (layouterEdge != null && !constellation.contains(layouterEdge)) {
                layouterEdgeByFig.remove(figEdge);
                addFigToConstellation(figEdge.getSourceFigNode(), constellation);
                addFigToConstellation(figEdge.getDestFigNode(), constellation);
                constellation.add(layouterEdge);
            }
        }
    }
    
    public void add(LayouterObject obj) {
        if (obj instanceof LayouterNode) {
            FigNode figNode = (FigNode) ((LayouterNode) obj).getContent();
            if (figNode == null) {
                throw new IllegalArgumentException("LayouterNode must contain a FigNode");
            }
            layouterNodeByFig.put(figNode, obj);
        } else if (obj instanceof LayouterEdge) {
            FigEdge figEdge = (FigEdge) ((LayouterEdge) obj).getContent();
            if (figEdge == null) {
                throw new IllegalArgumentException("LayouterEdge must contain a FigEdge");
            }
            layouterEdgeByFig.put(figEdge, obj);
        } else {
            throw new IllegalArgumentException("Need a LayouterNode or LayouterEdge");
        }
        objects.add(obj);
    }

    public List getObjects() {
        return objects;
    }

    public Rectangle getBounds() {
        Rectangle rect = null;
        for (Iterator it = getObjects().iterator(); it.hasNext(); ) {
            Object obj = it.next();
            if (obj instanceof LayouterNode) {
                if (rect == null) {
                    rect = ((LayouterNode) obj).getBounds();
                } else {
                    rect.add(((LayouterNode) obj).getBounds());
                }
            }
        }
        log.info("Getting bounds of " + getObjects().size() + " objects as " + rect);
        return rect;
    }
    
    public void translate(int dx, int dy) {
        log.info("Translating constellation by [" + getObjects().size() + "] " + dx + "," + dy);
        for (Iterator it = getObjects().iterator(); it.hasNext(); ) {
            LayouterObject obj = (LayouterObject) it.next();
            obj.translate(dx, dy);
        }
    }
    
    public Point getLocation() {
        return getBounds().getLocation();
    }
    
    public void setLocation(Point point) {
        Point oldPoint = getLocation();
        int dx = point.x - oldPoint.x;
        int dy = point.y - oldPoint.y;
        
        for (Iterator it = getObjects().iterator(); it.hasNext(); ) {
            LayouterObject obj = (LayouterObject) it.next();
            obj.translate(dx, dy);
        }
    }
}
