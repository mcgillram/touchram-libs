// $Id: SugiyamaLayouter.java 128 2006-08-29 13:59:33Z harrigan $
// Copyright (c) 2006 The Regents of the University of California. All
// Rights Reserved. Permission to use, copy, modify, and distribute this
// software and its documentation without fee, and without a written
// agreement is hereby granted, provided that the above copyright notice
// and this paragraph appear in all copies.  This software program and
// documentation are copyrighted by The Regents of the University of
// California. The software program and documentation are supplied "AS
// IS", without any accompanying services from The Regents. The Regents
// does not warrant that the operation of the program will be
// uninterrupted or error-free. The end-user understands that the program
// was developed for research purposes and is advised not to rely
// exclusively on the program for any reason.  IN NO EVENT SHALL THE
// UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
// SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS,
// ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
// THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF
// SUCH DAMAGE. THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
// PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
// CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
// UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

package org.tigris.geflayout.sugiyama;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tigris.geflayout.layout.Layouter;
import org.tigris.geflayout.layout.LayouterNode;
import org.tigris.geflayout.layout.LayouterObject;
import org.tigris.geflayout.sugiyama.coordinate_assignment.SimpleMethod;
import org.tigris.geflayout.sugiyama.crossing_reduction.SweepingMethod;
import org.tigris.geflayout.sugiyama.cycle_removal.EadesLinSmyth;
import org.tigris.geflayout.sugiyama.layer_assignment.LongestPath;
import org.tigris.geflayout.util.PairInt;
import org.tigris.geflayout.util.flow.FlowEdge;
import org.tigris.geflayout.util.flow.FlowGraph;
import org.tigris.geflayout.util.flow.MinCostFlow;

public class SugiyamaLayouter implements Layouter {
    
    /**
     * The logger.
     */
    private Log log = LogFactory.getLog(SugiyamaLayouter.class);
    
    /**
     * Default gap between nodes within a level.
     */
    public static int X_GAP = 100;
    
    /**
     * Default gap between levels.
     */
    public static int Y_GAP = 100;
    
    /**
     * An internal representation of the graph.
     */
    private SugiyamaGraph graph;

    /**
     * All edges in the layouter
     */
    private List edges;
    
    /**
     * All nodes in the layouter
     */
    private List nodes;
    
    /**
     * Construct a layouter.
     */    
    public SugiyamaLayouter() {
        edges = new ArrayList();
        nodes = new ArrayList();
        graph = new SugiyamaGraph();
    }
    
    /**
     * Add some object we want to layout.
     */
    public void add(LayouterObject obj) {
        if (obj instanceof SugiyamaNode) {
            log.info("Adding node " + ((SugiyamaNode) obj).getNode().getId());
            graph.addNode((SugiyamaNode) obj);
            nodes.add(obj);
        } else if (obj instanceof SugiyamaEdge) {
            //log.info("Adding edge " + ((SugiyamaEdge) obj).getEdge().getId());
            graph.addEdge((SugiyamaEdge) obj);
            edges.add(obj);            
        } else {
            // TODO Containers...
        }
    }

    /**
     * Remove some object we want to layout.
     */
    public void remove(LayouterObject obj) {
        // TODO Auto-generated method stub
    }
    
    /**
     * Determine if the layouter contains a particular layoutable object
     * @param obj the object to find
     * @return true if the object is known to the layouter
     */
    public boolean contains(LayouterObject obj) {
        return nodes.contains(obj) || edges.contains(obj);
    }

    /**
     * Get the objects we want to layout.
     */
    public List getObjects() {
        List objects = new ArrayList(nodes);
        objects.addAll(edges);
        return objects;
    }

    /**
     * Get an object we want to layout.
     */
    public LayouterObject getObject(int index) {
        // TODO Auto-generated method stub
        return null;
    }
      
    /**
     * Layout the graph; consists of five steps: temporarily remove any
     * directed cycles, assign the nodes to levels, reduce the number of
     * crossings, assign x-y coordinates to nodes and reroute edges.  
     */
    public void layout() {  
        log.info("Starting layout of " + graph.getNodeCount() + 
                " nodes and " + graph.getEdgeCount() + " edges");
        
       // EadesLinSmyth hcr = new EadesLinSmyth(graph);
        
        LongestPath l = new LongestPath(graph);
        makeProper();
        SweepingMethod cr = new SweepingMethod(graph, l.getDepth());
        SimpleMethod sca = new SimpleMethod(graph, l.getDepth(), X_GAP, Y_GAP);
        
        /* We are not using the edges so we do not use this method for Touch Ram */
       // routeEdges();
        
        graph = new SugiyamaGraph(); // reset all layout information for next time around
    }
    
    /**
     * Make the graph proper, i.e. insert dummy nodes so that no edge
     * spans more than one level.
     */
    private void makeProper() {
        log.info("Making the graph proper");
        
        for (Iterator it = edges.iterator(); it.hasNext(); ) {
            SugiyamaEdge edge = (SugiyamaEdge) it.next();
            SugiyamaNode tail = graph.getTail(edge);
            SugiyamaNode head = graph.getHead(edge);
            int span = tail.getLevel() - head.getLevel();
            if (span > 1) {
                graph.subdivideEdge(edge, span);
            }
        }
    }
    
    private void routeEdges() {
        for (Iterator it = edges.iterator(); it.hasNext(); ) {
            ((SugiyamaEdge) (it.next())).route();
        }
    }

    /**
     * Get the dimension of the graph.
     */
    public Dimension getMinimumDiagramSize() {
        return null;
    }
    
    public Rectangle getBounds() {
        Rectangle rect = null;
        for (Iterator it = getObjects().iterator(); it.hasNext(); ) {
            Object obj = it.next();
            if (obj instanceof LayouterNode) {
                if (rect == null) {
                    rect = ((LayouterNode) obj).getBounds();
                } else {
                    rect.add(((LayouterNode) obj).getBounds());
                }
            }
        }
        log.info("Getting bounds of " + getObjects().size() + " objects as " + rect);
        return rect;
    }
    
    public void translate(int dx, int dy) {
        for (Iterator it = getObjects().iterator(); it.hasNext(); ) {
            LayouterObject obj = (LayouterObject) it.next();
            obj.translate(dx, dy);
        }
    }
    
    public Point getLocation() {
        return getBounds().getLocation();
    }
    
    public void setLocation(Point point) {
        Point oldPoint = getLocation();
        int dx = point.x - oldPoint.x;
        int dy = point.y - oldPoint.y;
        
        for (Iterator it = getObjects().iterator(); it.hasNext(); ) {
            LayouterObject obj = (LayouterObject) it.next();
            obj.translate(dx, dy);
        }
    }
}
