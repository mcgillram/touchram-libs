// $Id: SugiyamaGraph.java 129 2006-08-29 16:21:02Z harrigan $
// Copyright (c) 2006 The Regents of the University of California. All
// Rights Reserved. Permission to use, copy, modify, and distribute this
// software and its documentation without fee, and without a written
// agreement is hereby granted, provided that the above copyright notice
// and this paragraph appear in all copies.  This software program and
// documentation are copyrighted by The Regents of the University of
// California. The software program and documentation are supplied "AS
// IS", without any accompanying services from The Regents. The Regents
// does not warrant that the operation of the program will be
// uninterrupted or error-free. The end-user understands that the program
// was developed for research purposes and is advised not to rely
// exclusively on the program for any reason.  IN NO EVENT SHALL THE
// UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
// SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS,
// ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
// THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF
// SUCH DAMAGE. THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
// PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
// CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
// UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

package org.tigris.geflayout.sugiyama;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.tigris.gef.presentation.FigEdge;

public class SugiyamaGraph {
    
    /**
     * The logger.
     */
    private Log log = LogFactory.getLog(SugiyamaGraph.class);
    
    /**
     * A map of vectors of SugiyamaNodes.
     */
    private HashMap adjList;
    
    /**
     * A map of FigNodes to SugiyamaNodes.
     */
    private HashMap figNodes;
    
    /**
     * Construct a graph.
     */
    public SugiyamaGraph() {
        adjList = new HashMap();
        figNodes = new HashMap();
    }
    
    /**
     * Adds a node to the graph we want to layout.
     * @param aNode
     */
    public void addNode(SugiyamaNode aNode) {
        if (!adjList.containsKey(aNode)) {
            adjList.put(aNode, new Vector());
            figNodes.put(aNode.getNode(), aNode);
        }
    }
    
    /**
     * Adds an edge to the graph we want to layout.
     * @param anEdge
     */
    public void addEdge(SugiyamaEdge anEdge) {
        if (adjList.containsKey(getTail(anEdge)) && adjList.containsKey(getHead(anEdge))) { 
            Vector adj = (Vector) adjList.get(getTail(anEdge));
            adj.addElement(getHead(anEdge));
        }
    }
    
    /**
     * Inserts dummy nodes along an edge.
     * @param anEdge
     */
    public void subdivideEdge(SugiyamaEdge anEdge, int span) {
        Vector adj = (Vector) adjList.get(getTail(anEdge));
        adj.remove(getHead(anEdge));
        
        SugiyamaNode prev = getTail(anEdge);
        for(int i = 1; i < span; i++) {
            log.info("Adding a dummy node");
            
            SugiyamaNode dummy = new SugiyamaNode(anEdge);
            dummy.setLevel(getTail(anEdge).getLevel() - i);
            ((Vector) adjList.get(prev)).add(dummy);
            adjList.put(dummy, new Vector());
            prev = dummy;
        }
        
        ((Vector) adjList.get(prev)).add(getHead(anEdge));
    }
    
    /**
     * Gets the node in which the edge originates.
     * @param anEdge
     * @return
     */
    public SugiyamaNode getHead(SugiyamaEdge anEdge) {
        FigEdge e = anEdge.getEdge();
        if (e != null) {
            return (SugiyamaNode) figNodes.get(e.getDestFigNode());
        } else {
            return anEdge.getHead();
        }
    }
    
    /**
     * Gets the node in which the edge terminates.
     * @param anEdge
     * @return
     */
    public SugiyamaNode getTail(SugiyamaEdge anEdge) {
        FigEdge e = anEdge.getEdge();
        if (e != null) {
            return (SugiyamaNode) figNodes.get(e.getSourceFigNode());
        } else {
            return anEdge.getTail();
        }
    }
    
    /**
     * Get the incoming neighbours of a node.
     * @param aNode
     * @return
     */
    public Vector getIncomingNeighbours(SugiyamaNode aNode) {
        Vector incoming = new Vector();
        Iterator it = adjList.keySet().iterator();
        while (it.hasNext()) {
            SugiyamaNode node = (SugiyamaNode) it.next();
            if (((Vector)adjList.get(node)).contains(aNode)) {
                incoming.addElement(node);
            }
        }
        return incoming;
    }
    
    /**
     * Get the outgoing neighbours of a node.
     * @param aNode
     * @return
     */
    public Vector getOutgoingNeighbours(SugiyamaNode aNode) {
        return (Vector)adjList.get(aNode);
    }
    
    /**
     * Get the incoming and outgoing neighbours of a node.
     * @param aNode
     * @return
     */
    public Vector getNeighbours(SugiyamaNode aNode) {
        Vector neighbours = new Vector();
        neighbours.addAll(getIncomingNeighbours(aNode));
        neighbours.addAll(getOutgoingNeighbours(aNode));
        return neighbours;
    }
    
    /**
     * Get the number of incoming neighbours of a node.
     * @param aNode
     * @return
     */
    public int getIndegree(SugiyamaNode aNode) {
        return getIncomingNeighbours(aNode).size();
    }
    
    /**
     * Get the number of incoming neighbours of a node.
     * @param aNode
     * @return
     */
    public int getOutdegree(SugiyamaNode aNode) {
        return getOutgoingNeighbours(aNode).size();
    }
    
    /**
     * Get the number of incoming and outgoing neighbours of a node.
     * @param aNode
     * @return
     */
    public int getDegree(SugiyamaNode aNode) {
        return getIndegree(aNode) + getOutdegree(aNode);
    }
    
    /**
     * Get all the nodes (including dummies).
     * @return
     */
    public Vector getNodes() {
        return new Vector(adjList.keySet());
    }
    
    /**
     * Get the number of nodes in the graph.
     * @return
     */
    public int getNodeCount() {
        return adjList.keySet().size();
    }
    
    /**
     * Get the number of edges in the graph.
     * @return
     */
    public int getEdgeCount() {
        int count = 0;
        Iterator it = adjList.keySet().iterator();
        while (it.hasNext()) {
            Vector nodes = (Vector) adjList.get(it.next());
            count += nodes.size();
        }
        return count;
    }
    
    /**
     * Get all nodes whose indegree is zero.
     * @return
     */
    public Vector getSources() {
        Vector sources = new Vector();
        Iterator it = adjList.keySet().iterator();
        while (it.hasNext()) {
            SugiyamaNode node = (SugiyamaNode) it.next();
            if (getIndegree(node) == 0) {
                sources.add(node);
            }
        }
        return sources;
    }
    
    /**
     * Get all nodes whose outdegree is zero.
     * @return
     */
    public Vector getSinks() {
        Vector sinks = new Vector();
        Iterator it = adjList.keySet().iterator();
        while (it.hasNext()) {
            SugiyamaNode node = (SugiyamaNode) it.next();
            if (getOutdegree(node) == 0) {
                sinks.add(node);
            }
        }
        return sinks;
    }
    
    /**
     * Reverse an edge.
     * @param tail
     * @param head
     */
    public void reverseEdge(SugiyamaNode tail, SugiyamaNode head) {
        ((Vector)adjList.get(head)).addElement(tail);
        ((Vector)adjList.get(tail)).removeElement(head);      
    }
    
    /**
     * Get nodes on the given level.
     * @return
     */
    public Vector getLevel(int level) {
        int order = 1;
        Vector nodes = new Vector();
        for (Iterator it = adjList.keySet().iterator(); it.hasNext(); ) {
            SugiyamaNode node = (SugiyamaNode) it.next();
            if (node.getLevel() == level) {
                nodes.addElement(node);
                if (node.getOrder() == 0) {
                    node.setOrder(order++);
                }
            }
        }
        Collections.sort(nodes, new SugiyamaNodeComparator());
        return nodes;
    }
    
    /**
     * Mark all nodes the same way.
     * @param marked
     */
    public void setMarked(boolean marked) {
        for (Iterator it = adjList.keySet().iterator(); it.hasNext(); ) {
            SugiyamaNode node = (SugiyamaNode) it.next();
            node.setMarked(marked);
        }
    }
}
