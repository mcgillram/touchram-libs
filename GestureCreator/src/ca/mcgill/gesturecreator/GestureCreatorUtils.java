package ca.mcgill.gesturecreator;

import java.util.ArrayList;
import java.util.List;

import org.mt4j.util.math.Vector3D;
import org.mt4j.util.math.Vertex;

public class GestureCreatorUtils {

    public static final String OUTPUT_LOCATION = "output.txt";
    public static final String SAVE_BUTTON_LOCATION = "images/savebutton.png";
    public static final String TEST_BUTTON_LOCATION = "images/testgestbutton.png";

    static List<Vector3D> convertVerticesToVector3DList(Vertex[] vertices) {
        List<Vector3D> points = new ArrayList<Vector3D>();
        for (Vertex v : vertices) {
            points.add(v.getCopy());
        }
        return points;
    }
}
