## How to add Gestures to MT4J ##

1. Use the GestureCreator to create a gesture and test it
2. Save the gesture to output.txt
3. Go to org.mt4j.input.inputProcessors.componentProcessors.unistrokeProcessor
4. In UnistrokeProcessor, add the name of your new gesture to the enum UnistrokeGesture
5. In UnistrokeTemplate, go to the method addTemplate and add the following case to the switch/case:
    		case <new enum>:
			Vector3D[] some_array = <generated vector3d points from the output.txt>;
		points = Arrays.asList(some_array);
6. Compile MT4j